package io.atlassian.myservice.application;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

public class SwaggerIntegrationTest extends MyServiceIntegrationTestBase {
    private static final Logger log = LoggerFactory.getLogger(SwaggerIntegrationTest.class);

//    @Test
//    public void testSwaggerJson() throws Exception {
//        final Response response = client.target(APP_CONTEXT + "swagger.json")
//                .request()
//                .get();
//        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
//        ObjectNode swagger = response.readEntity(ObjectNode.class);
//        log.info("Got this swagger body: {}", swagger);
//
//        JsonNode swaggerPaths = swagger.get("paths");
//        assertThat(swaggerPaths.get("/docs").fieldNames()).containsAll(ImmutableSet.of("post"));
//        assertThat(swaggerPaths.get("/docs/{id}").fieldNames()).containsAll(ImmutableSet.of("put", "get", "delete"));
//    }
}

