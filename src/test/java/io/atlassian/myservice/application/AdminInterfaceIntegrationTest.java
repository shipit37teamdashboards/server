package io.atlassian.myservice.application;

import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

public class AdminInterfaceIntegrationTest extends MyServiceIntegrationTestBase {

    @Test
    public void testPing() throws Exception {
        final Response response = client.target(ADMIN_CONTEXT + "/ping")
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    public void testHealthcheck() throws Exception {
        final Response response = client.target(ADMIN_CONTEXT + "/healthcheck")
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }
}

