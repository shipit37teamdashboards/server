package io.atlassian.myservice.application;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.client.http.AuthorizationHeaderGeneratorImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.atlassian.myservice.config.MyServiceConfiguration;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;

/**
 * A base class for tests Integration Tests which starts the dropwizard server once for the whole class.
 * Subclasses can use {@link #APP_CONTEXT} and {@link #ADMIN_CONTEXT} as base URLs to access the service,
 * and the Jersey {@link #client} to make REST requests.
 */
public class MyServiceIntegrationTestBase {
    /** Base URL for accessing the app.  Ends with slash. */
    @SuppressFBWarnings("MS_CANNOT_BE_FINAL")
    protected static String APP_CONTEXT;
    /** Base URL for accessing the dropwizard admin service. Ends with slash. */
    @SuppressFBWarnings("MS_PKGPROTECT")
    protected static String ADMIN_CONTEXT;

    private static final String CONFIG_PATH =
            ResourceHelpers.resourceFilePath("shipit-beehives.yaml") + "," +
            ResourceHelpers.resourceFilePath("shipit-beehives-test.yaml");
    private static final String URL_TEMPLATE = "http://localhost:%d%s";

    /**
     * Starts the MyService application.
     */
    @ClassRule
    public static final DropwizardAppRule<MyServiceConfiguration> APP_RULE =
            new DropwizardAppRule<>(MyServiceApplication.class, CONFIG_PATH);

    /** Jersey client to use for REST calls */
    protected Client client;

    @BeforeClass
    public static void setupBeforeClass() throws Exception {
        ADMIN_CONTEXT = String.format(
                URL_TEMPLATE,
                APP_RULE.getAdminPort(),
                APP_RULE.getEnvironment().getAdminContext().getContextPath());
        APP_CONTEXT = String.format(
                URL_TEMPLATE,
                APP_RULE.getLocalPort(),
                APP_RULE.getEnvironment().getApplicationContext().getContextPath());
    }

    @Before
    public void setUpBeforeTest() throws Exception {
        client = ClientBuilder.newClient();
    }

    @After
    public void tearDown() throws Exception {
        client.close();
    }

    /**
     * @return the value that should be set as the {@link AUTHORIZATION} header value
     */
    protected String asapHeaderValue(String issuer) throws InvalidTokenException, CannotRetrieveKeyException {
        URI privateKeyPath = URI.create("classpath:/asap_private_keys/");
        AuthorizationHeaderGenerator generator = AuthorizationHeaderGeneratorImpl.createDefault(privateKeyPath);
        String audience = "shipit-beehives";
        String keyId = issuer + "/key.pem";
        Jwt jwt = JwtBuilder.newJwt()
                .audience(audience)
                .issuer(issuer)
                .keyId(keyId)
                .expirationTime(Instant.now().plusSeconds(Duration.ofMinutes(30).getSeconds()))
                .build();
        return generator.generateAuthorizationHeader(jwt);
    }
}

