package io.atlassian.myservice.util;

import org.junit.Test;

import static io.atlassian.myservice.util.DateUtil.parseDate;
import static io.atlassian.myservice.util.DateUtil.parseDayOfYear;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Ilya Sadykov
 */
public class DateUtilTest {

    @Test
    public void parseDateShouldBeOK() throws Exception {
        assertThat(parseDate("2016-12-06T18:28:20.000-0600").getTime(), equalTo(1481070500000L));
    }

    @Test
    public void parseDayOfYearDateShouldBeOK() throws Exception {
        assertThat(parseDayOfYear("2017-01-27").getTime(), equalTo(1485435600000L));
    }
}