package io.atlassian.myservice.util;

import org.junit.Test;

import static io.atlassian.myservice.util.StringUtil.removeTags;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Ilya Sadykov
 */
public class StringUtilTest {


    @Test
    public void removeTagsShouldCleanStringFromTags() throws Exception {
        assertThat(
                removeTags("<span class='status-macro aui-lozenge aui-lozenge-success'>On Track</span>"),
                equalTo("On Track")
        );
    }
}