package io.atlassian.myservice.clients;

import io.atlassian.myservice.clients.atlaseye.AtlasEyeClient;
import io.atlassian.myservice.clients.atlaseye.model.RepositoryItem;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by akoshelev on 12/8/16.
 */
public class AtlasEyeTest {
    @Test
    public void returnsReposList() {
        List<RepositoryItem> testRepos = AtlasEyeClient.getReposForTeam();
        assertThat(testRepos.size(), not(0));
        testRepos.forEach(repositoryItem -> System.out.println(repositoryItem.getDisplayName()));
    }
}
