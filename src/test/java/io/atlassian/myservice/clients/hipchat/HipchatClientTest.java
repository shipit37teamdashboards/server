package io.atlassian.myservice.clients.hipchat;

import ch.viascom.hipchat.api.exception.APIException;
import org.junit.Test;

import java.util.concurrent.ExecutionException;

/**
 * Created by akoshelev on 12/8/16.
 */
public class HipchatClientTest {
    @Test
    public void getRoomForTeamReturnsThreeRoomNames() throws APIException {
        HipChatIntegration hipChat = new HipChatIntegration();

        for (String roomName: hipChat.getRoomsForTeam(null)) {
            System.out.println(">>> " + roomName);
        }
    }

    @Test
    public void getLastActiveReturnsDate() throws InterruptedException, ExecutionException, APIException {
        HipChatIntegration hipChat = new HipChatIntegration();
        System.out.println(">>> " + hipChat.getLastActive("akoshelev@atlassian.com"));
    }

}
