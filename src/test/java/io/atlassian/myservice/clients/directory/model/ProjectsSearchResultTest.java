package io.atlassian.myservice.clients.directory.model;

import io.atlassian.myservice.clients.projectcentral.model.ProjectsSearchResult;
import io.atlassian.myservice.util.JsonUtil;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.apache.commons.io.FileUtils.readFileToString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Ilya Sadykov
 */
public class ProjectsSearchResultTest {

    private final String pcResponseJson;

    public ProjectsSearchResultTest() throws IOException {
        pcResponseJson = readFileToString(new File(getClass().getClassLoader().getResource("project-central-response.json").getFile()));
    }

    @Test
    public void searchProjectsResultShouldBeDeserialized() throws Exception {
        final ProjectsSearchResult res = JsonUtil.fromJson(pcResponseJson, ProjectsSearchResult.class);
        assertThat(res.getProjects().size(), equalTo(3));
        assertThat(res.getProjects().get(0).getFields().getTrackStatus(), equalTo("On Track"));
        assertThat(res.getProjects().get(0).getFields().getStatus().getName(), equalTo("Pending"));
        assertThat(res.getProjects().get(0).getFields().getSummary(), equalTo("Safer startup for Confluence (Safe mode)"));
    }
}