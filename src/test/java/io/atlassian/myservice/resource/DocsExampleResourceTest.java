package io.atlassian.myservice.resource;

import io.atlassian.myservice.controller.ExampleDocId;
import io.atlassian.myservice.controller.ExampleDocument;
import io.atlassian.myservice.controller.ExampleDocumentController;
import io.atlassian.myservice.controller.ExampleDocumentControllerImpl;
import io.atlassian.myservice.resource.DocsExampleResource.ExampleDocumentResponse;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nullable;
import javax.ws.rs.WebApplicationException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for {@link io.atlassian.myservice.resource.DocsExampleResource}.
 */
// EXAMPLE NOTES: This calls methods of the DocsExampleResource directly without setting up any Jersey context, so it
// can only test methods that don't require any Jersey magic.
// To test using jerysey magic but without doing full http integration tests, you can use
// io.dropwizard.testing.junit.ResourceTestRule.
public class DocsExampleResourceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private DocsExampleResource resource;

    @Before
    public void setUp() throws Exception {
        resource = new DocsExampleResource(new ExampleDocumentControllerImpl());
    }

    @Test
    public void createShouldReturnExampleDocument() throws Exception {
        ExampleDocument document = new ExampleDocument("AUTHOR", "CONTENT");
        ExampleDocumentResponse documentResponse = resource.createExampleDocumentInternal(document);
        assertThat(documentResponse.getId()).isEqualTo(new ExampleDocId(1L));
        assertThat(documentResponse.getDoc().getContent()).isEqualTo("CONTENT");
        assertThat(documentResponse.getDoc().getAuthor()).isEqualTo("AUTHOR");
    }

    @Test
    public void getShouldReturnCreatedExampleDocument() throws Exception {
        ExampleDocument document = new ExampleDocument("AUTHOR", "CONTENT");
        ExampleDocumentResponse documentResponse = resource.createExampleDocumentInternal(document);
        ExampleDocId createdId = documentResponse.getId();

        Optional<ExampleDocumentResponse> getResponse = resource.getExampleDocument(createdId);
        assertThat(getResponse).isPresent();
        assertThat(getResponse.get()).isEqualToComparingFieldByFieldRecursively(documentResponse);
    }

    @Test
    public void getAbsentDocShouldReturnAbsent() throws Exception {
        // All valid docs have ids >= 1000
        assertThat(resource.getExampleDocument(new ExampleDocId(1))).isEmpty();
    }

    @Test
    public void updateNonExistingDocShouldThrow() throws Exception {
        thrown.expect(WebApplicationException.class);
        thrown.expectMessage("Can't update document 1: it does not exist");
        thrown.expect(new WebApplicationExceptionStatusMatcher(404));
        // All valid docs have ids >= 1000
        resource.updateExampleDocument(new ExampleDocId(1), new ExampleDocument("AUTHOR2", "CONTENT2"));
    }

    private static class WebApplicationExceptionStatusMatcher extends BaseMatcher<WebApplicationException> {
        private int expectedStatus;

        WebApplicationExceptionStatusMatcher(int expectedStatus) {
            this.expectedStatus = expectedStatus;
        }

        @Override
        public boolean matches(Object exception) {
            return exception instanceof WebApplicationException
                    && ((WebApplicationException) exception).getResponse().getStatus() == expectedStatus;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("WebApplicationException with status " + expectedStatus);
        }
    }


}

