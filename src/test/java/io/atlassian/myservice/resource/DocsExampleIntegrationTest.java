package io.atlassian.myservice.resource;

import com.google.common.collect.ImmutableMap;
import io.atlassian.myservice.application.MyServiceIntegrationTestBase;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for the REST API of {@link DocsExampleResource}.
 */
public class DocsExampleIntegrationTest extends MyServiceIntegrationTestBase {

    private static final ImmutableMap<String, String> DEFAULT_DOC = ImmutableMap.of(
            "author", "AUTHOR",
            "content", "CONTENT");

//    @Test
//    public void createShouldReturnExampleDocument() throws Exception {
//        Map<String, String> request = DEFAULT_DOC;
//
//        final Response jerseyResponse = client.target(APP_CONTEXT + "docs")
//                .request()
//                .header(AUTHORIZATION, asapHeaderValue("tester"))
//                .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE));
//        assertThat(jerseyResponse.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
//        HashMap response = jerseyResponse.readEntity(HashMap.class);
//        HashMap doc = (HashMap) response.get("doc");
//
//        assertThat(((int) response.get("id"))).isGreaterThanOrEqualTo(1);
//        assertThat(doc).isEqualTo(DEFAULT_DOC);
//    }
//
//    @Test
//    public void getShouldReturnCreatedExampleDocument() throws Exception {
//        long createdId = createDefaultDoc();
//
//        Response jerseyResponse = client.target(APP_CONTEXT + "docs/" + createdId)
//                .request()
//                .header(AUTHORIZATION, asapHeaderValue("tester"))
//                .get();
//        assertThat(jerseyResponse.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
//        HashMap response = jerseyResponse.readEntity(HashMap.class);
//        HashMap doc = (HashMap) response.get("doc");
//        assertThat(doc).isEqualTo(DEFAULT_DOC);
//    }
//
//    @Test
//    public void getAbsentDocShouldReturn404() throws Exception {
//        Response jerseyResponse = client.target(APP_CONTEXT + "docs/9999")
//                .request()
//                .header(AUTHORIZATION, asapHeaderValue("tester"))
//                .get();
//        assertThat(jerseyResponse.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
//    }
//
////    @Test
////    public void getWithoutAsapShouldBeUnauthorized() throws Exception {
////        Response jerseyResponse = client.target(APP_CONTEXT + "docs/1")
////                .request().get();
////        assertThat(jerseyResponse.getStatus()).isEqualTo(Response.Status.UNAUTHORIZED.getStatusCode());
////    }
//
//    // "otheruser" is not authorized to write
//    @Test
//    public void createByOtherUserShouldBeForbidden() throws Exception {
//        Map<String, String> request = DEFAULT_DOC;
//        Response jerseyResponse = client.target(APP_CONTEXT + "docs")
//                .request()
//                .header(AUTHORIZATION, asapHeaderValue("otheruser"))
//                .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE));
//        assertThat(jerseyResponse.getStatus()).isEqualTo(Response.Status.FORBIDDEN.getStatusCode());
//    }
//
//    // "otheruser" is not authorized to write; but all users can read
//    @Test
//    public void getByOtherUserShouldBeAllowed() throws Exception {
//        long createdId = createDefaultDoc();
//        Response jerseyResponse = client.target(APP_CONTEXT + "docs/" + createdId)
//                .request()
//                .header(AUTHORIZATION, asapHeaderValue("otheruser"))
//                .get();
//        assertThat(jerseyResponse.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
//    }
//
//    /**
//     * Creates DEFAULT_DOC with a "tester" post to /docs and returns the doc id.
//     *
//     * @return the doc id of created doc
//     * @throws Exception or AssertionError on any failure to create doc
//     */
//    private long createDefaultDoc() throws Exception {
//        Map<String, String> request = DEFAULT_DOC;
//        Response jerseyResponse = client.target(APP_CONTEXT + "docs")
//                .request()
//                .header(AUTHORIZATION, asapHeaderValue("tester"))
//                .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE));
//        assertThat(jerseyResponse.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
//        HashMap response = jerseyResponse.readEntity(HashMap.class);
//        return (int) response.get("id");
//    }
}

