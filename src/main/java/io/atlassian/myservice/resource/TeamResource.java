package io.atlassian.myservice.resource;

import com.google.inject.Inject;
import io.atlassian.myservice.clients.directory.DirectoryClient;
import io.atlassian.myservice.clients.directory.model.Employee;
import io.atlassian.myservice.clients.directory.model.TeamMemberList;
import io.atlassian.myservice.clients.hipchat.HipChatIntegration;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/teams")
public class TeamResource {

    private final DirectoryClient directoryClient;
    private final HipChatIntegration hipchatClient;

    @Inject
    public TeamResource(DirectoryClient directoryClient, HipChatIntegration hipchatClient) {
        this.directoryClient = directoryClient;
        this.hipchatClient = hipchatClient;
    }

    @GET
    @Path("{teamId}")
    @Produces(APPLICATION_JSON)
    public Response getTeam(@PathParam("teamId") long teamId) throws IOException {

        TeamMemberList team = directoryClient.getTeamMembers(teamId).execute().body();
        Employee firstEmployee = team.getResults().get(0);
        String polygon = "m170,65 l30,0 l0,60 l-30,0 l0,-60 z";
        Location location = new Location(
                "https://rps-bucket-prod-east.s3.amazonaws.com/directory-services/directory-avatar/plan/level-2-floorplan.png",
                "Sydney - 341 George St - Level 2", polygon);

        List<TeamMember> teamMembers = new ArrayList<>();
        for (Employee employee : team.getResults()) {
            User manager = new User(employee.getManagerUsername(), employee.getManagerFullName());
            TeamMember teamMember = new TeamMember(employee.getFullName(),
                    employee.getUsername(),
                    getPhotoUrl(employee.getUsername()),
                    employee.getJobTitle().getName(), "Java", new AbsenseStatus(
                    AbsenseType.AVAILABLE, "In the office"),
                    location, HipchatStatus.ONLINE, employee.getHireDate(), manager);
            teamMembers.add(teamMember);
        }

        TeamResponse teamResponse = new TeamResponse(
                firstEmployee.getTeam().getId(),
                firstEmployee.getTeam().getName(),
                "Deployment workflow pipeline; Deployment infrastructure services",
                teamMembers,
                location,
                hipchatClient.getRoomsForTeam(team)
        );

        return Response.ok() //200
                .entity(teamResponse)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .allow("OPTIONS").build();
    }

    private static String getPhotoUrl(String username) {
        return String.format("https://rps-bucket-prod-east.s3.amazonaws.com/directory-services/directory-avatar/256x256/%s.png", username);
    }

    public static class TeamResponse {
        private final long teamId;
        private final String name;
        private final String description;
        private final List<TeamMember> members;
        private final Location location;
        private final List<String> hipchatRooms;

        public TeamResponse(long teamId, String name, String description, List<TeamMember> members, Location location, List<String> hipchatRooms) {
            this.teamId = teamId;
            this.name = name;
            this.description = description;
            this.members = members;
            this.location = location;
            this.hipchatRooms = hipchatRooms;
        }

        public long getTeamId() {
            return teamId;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public List<TeamMember> getMembers() {
            return members;
        }

        public Location getLocation() {
            return location;
        }

        public List<String> getHipchatRooms() {
            return hipchatRooms;
        }
    }

    public static class TeamMember {
        private final String name;
        private final String username;
        private final String photoUrl;
        private final String position;
        private final String specialization;
        private final AbsenseStatus absenseStatus;
        private final Location location;
        private final HipchatStatus hipchatStatus;
        private final String hireDate;
        private final User manager;

        public TeamMember(String name, String username, String photoUrl, String position, String specialization, AbsenseStatus absenseStatus, Location location, HipchatStatus hipchatStatus, String hireDate, User manager) {
            this.name = name;
            this.username = username;
            this.photoUrl = photoUrl;
            this.position = position;
            this.specialization = specialization;
            this.absenseStatus = absenseStatus;
            this.location = location;
            this.hipchatStatus = hipchatStatus;
            this.hireDate = hireDate;
            this.manager = manager;
        }

        public String getName() {
            return name;
        }

        public String getUsername() {
            return username;
        }

        public String getPhotoUrl() {
            return photoUrl;
        }

        public String getPosition() {
            return position;
        }

        public String getSpecialization() {
            return specialization;
        }

        public AbsenseStatus getAbsenseStatus() {
            return absenseStatus;
        }

        public Location getLocation() {
            return location;
        }

        public HipchatStatus getHipchatStatus() {
            return hipchatStatus;
        }

        public String getHireDate() {
            return hireDate;
        }

        public User getManager() {
            return manager;
        }
    }

    public static class AbsenseStatus {
        private final AbsenseType type;
        private final String title;

        public AbsenseStatus(AbsenseType type, String title) {
            this.type = type;
            this.title = title;
        }

        public AbsenseType getType() {
            return type;
        }

        public String getTitle() {
            return title;
        }
    }

    public enum AbsenseType {
        AVAILABLE,
        VACATION,
        BUSINESS_TRIP,
    }

    public static class Location {
        private final String floorPlanUrl;
        private final String floorTitle;
        private final String polygon;

        public Location(String floorPlanUrl, String floorTitle, String polygon) {
            this.floorPlanUrl = floorPlanUrl;
            this.floorTitle = floorTitle;
            this.polygon = polygon;
        }

        public String getFloorPlanUrl() {
            return floorPlanUrl;
        }

        public String getFloorTitle() {
            return floorTitle;
        }

        public String getPolygon() {
            return polygon;
        }
    }

    public enum HipchatStatus {
        OFFLINE,
        ONLINE,
        AWAY
    }

    public static class User {
        private final String username;
        private final String fullName;

        public User(String username, String fullName) {
            this.username = username;
            this.fullName = fullName;
        }

        public String getUsername() {
            return username;
        }

        public String getFullName() {
            return fullName;
        }
    }
}
