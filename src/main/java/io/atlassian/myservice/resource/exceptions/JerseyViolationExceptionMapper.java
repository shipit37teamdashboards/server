package io.atlassian.myservice.resource.exceptions;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import io.atlassian.myservice.resource.entities.ErrorOutput;
import io.dropwizard.jersey.validation.ConstraintMessage;
import io.dropwizard.jersey.validation.JerseyViolationException;
import org.glassfish.jersey.server.model.Invocable;

import javax.validation.ConstraintViolation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.Set;

public class JerseyViolationExceptionMapper implements ExceptionMapper<JerseyViolationException> {
    // copied from io.dropwizard.jersey.validation
    // modified to return our types

    @Override
    public Response toResponse(final JerseyViolationException exception) {
        final Set<ConstraintViolation<?>> violations = exception.getConstraintViolations();
        final Invocable invocable = exception.getInvocable();
        final ImmutableList<String> errors = FluentIterable.from(exception.getConstraintViolations())
                .transform(violation -> ConstraintMessage.getMessage(violation, invocable)).toList();

        final int status = ConstraintMessage.determineStatus(violations, invocable);

        return Response.status(status)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(new ErrorOutput(Response.Status.fromStatusCode(status), errors))
                .build();
    }
}
