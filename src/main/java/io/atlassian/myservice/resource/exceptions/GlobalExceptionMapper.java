package io.atlassian.myservice.resource.exceptions;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.atlassian.myservice.resource.entities.ErrorOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.Collections;
import java.util.List;

public class GlobalExceptionMapper implements ExceptionMapper<Exception> {
    private final Logger log = LoggerFactory.getLogger(GlobalExceptionMapper.class);

    @Override
    public Response toResponse(Exception exception) {
        Response.StatusType status = Response.Status.INTERNAL_SERVER_ERROR;

        if (exception instanceof JsonGenerationException) {
            log.warn("Error generating JSON", exception);
            return Response.status(status)
                    .entity(new ErrorOutput(exception, status))
                    .build();
        }

        if (exception instanceof JsonProcessingException) {
            return handleException((JsonProcessingException) exception);
        }

        // Handle the generic exception
        if (exception instanceof WebApplicationException) {
            WebApplicationException webApplicationException = (WebApplicationException) exception;
            status = webApplicationException.getResponse().getStatusInfo();
            if (status.getFamily().equals(Response.Status.Family.REDIRECTION)) {
                return webApplicationException.getResponse();
            }
        }

        if (status.getFamily().compareTo(Response.Status.Family.SERVER_ERROR) >= 0) {
            log.error("Uncaught exception handled by ExceptionMapper!", exception);
            return Response.status(status)
                    .entity(new ErrorOutput(exception, status))
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .build();
        }
        else {
            return Response.status(status)
                    .entity(new ErrorOutput(status, Collections.singletonList(exception.toString())))
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .build();
        }
    }

    /**
     * Handles JSON exceptions of some sort.
     * @param exception The exception
     * @return An appropriate response
     */
    private Response handleException(final JsonProcessingException exception) {
        // By default, this prints the original message + location in the same message
        // We want to split this out
        List<String> messages;

        // Handle the cause message if it exists
        if (exception.getCause() != null) {
            messages = Collections.singletonList(exception.getCause().getLocalizedMessage());
        }
        else if (exception.getLocation() == null) {
            messages = Collections.singletonList(exception.getOriginalMessage());
        }
        else {
            // Manually construct a cool message
            messages = Collections.singletonList(String.format("On line: %d, column: %d; %s",
                    exception.getLocation().getLineNr(), exception.getLocation().getColumnNr(), exception.getOriginalMessage()));
        }

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorOutput(Response.Status.BAD_REQUEST, messages))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
