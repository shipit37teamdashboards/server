package io.atlassian.myservice.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import io.atlassian.myservice.clients.atlaseye.AtlasEyeClient;
import io.atlassian.myservice.clients.atlaseye.model.RepositoryItem;
import io.atlassian.myservice.config.MyServiceConfiguration;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import java.util.List;
import java.util.Optional;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Created by akoshelev on 12/9/16.
 */
@Path("/repositories")
public class RepositoriesResource {
    private final ObjectMapper objectMapper;
    private final MyServiceConfiguration configuration;

    @Inject
    public RepositoriesResource(ObjectMapper objectMapper, MyServiceConfiguration configuration) {
        this.objectMapper = objectMapper;
        this.configuration = configuration;
    }

    @GET
    @Produces(APPLICATION_JSON)
    public Optional<List<RepositoryItem>> getRepositories() {
        return Optional.of(AtlasEyeClient.getReposForTeam());
    }
}
