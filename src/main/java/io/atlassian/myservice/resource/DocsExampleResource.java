package io.atlassian.myservice.resource;

import com.atlassian.asap.core.server.jersey.Asap;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.annotations.VisibleForTesting;
import io.atlassian.myservice.controller.ExampleDocId;
import io.atlassian.myservice.controller.ExampleDocument;
import io.atlassian.myservice.controller.ExampleDocumentController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Optional;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;

/**
 * This class serves as an example of how to map a REST resource into Jersey.
 */
// EXAMPLE NOTES:
// * You couldn't use this in production use because it keeps state from request-to-request, but that state
//   isn't shared in a database between instances of this application
// * This demonstrates
//   * Jackson annotations to facilitate JSON serialization and deserialization of entities
//   * Automatic conversion of input and output into entity types by Jersey + Jackson
//   * Jersey (JAX-RS) annotations to map methods to URI paths + HTTP methods
//   * Swagger annotations to provide additional documentation of the api
//   * ASAP enforcement:
//      * Read methods require valid ASAP token for any issuer
//      * Write methods require ASAP token with issuer="tester"
@Api(value = "docs", description = "Operations on ExampleDocuments")
@Path("/docs")
@Produces(APPLICATION_JSON)
@Asap
public class DocsExampleResource {
    private static final Logger log = LoggerFactory.getLogger(DocsExampleResource.class);

    private final ExampleDocumentController controller;

    public DocsExampleResource(final ExampleDocumentController controller) {
        this.controller = controller;
    }

    @POST
    @Consumes(APPLICATION_JSON)
    // EXAMPLE NOTES: Since this method returns Response instead of ExampleDocumentResponse, we use a swagger annotation
    // to document the expected response model.
    @ApiOperation(value = "Creates a new document",
            notes = "Returns the document and the ID of the created document",
            code = SC_CREATED,
            response = ExampleDocumentResponse.class)
    // EXAMPLE NOTE: We should document response code(s) with an @ApiResponses annotation
    @ApiResponses({
        @ApiResponse(code = SC_CREATED, message = "When document is created"),
    })
    @Asap(authorizedIssuers = "tester")
    public Response createExampleDocument(
            @Context UriInfo uriInfo,
            ExampleDocument doc) throws JsonProcessingException {
        ExampleDocumentResponse docResponse = createExampleDocumentInternal(doc);
        return Response.created(uriInfo.getAbsolutePathBuilder().path(String.valueOf(docResponse.getId())).build())
                .entity(docResponse)
                .build();
    }

    // EXAMPLE NOTE: Simple unit tests can't test createExampleDocument() directly because it uses uriInfo, so we extract
    // most of the guts of createExampleDocument into a helper method that can be unit tested.
    @VisibleForTesting
    ExampleDocumentResponse createExampleDocumentInternal(ExampleDocument doc) {
        log.debug("Creating document");
        ExampleDocId docId = controller.insert(doc);
        log.debug("Created document with id {}", docId);

        return new ExampleDocumentResponse()
                .setDoc(doc)
                .setId(docId);
    }

    /**
     * Retrieves an existing document.
     *
     * <p>REST API note: returns 404 Not Found on no-such-document;
     * 200 with the document content if the doc exists.
     *
     * @param docId ID of document to retrieve
     * @return document and id of document, if it exists
     * @throws WebApplicationException on not found
     */
    @GET @Path("{id: [0-9]+}")
    @ApiOperation("Retrieves an existing document")
    // EXAMPLE NOTE: We should document response code(s) with an @ApiResponses annotation
    @ApiResponses({
        @ApiResponse(code = SC_OK, message = "When document is found"),
        @ApiResponse(code = SC_NOT_FOUND, message = "When document does not exist"),
    })
    public Optional<ExampleDocumentResponse> getExampleDocument(@PathParam("id") ExampleDocId docId) {
        log.debug("Loading docId {}", docId);
        Optional<ExampleDocument> doc = Optional.ofNullable(controller.get(docId));
        return doc.map(d -> new ExampleDocumentResponse().setId(docId).setDoc(d));
    }

    /**
     * Delete a document.  Succeeds whether or not the document already existed.
     *
     * <p>REST API note: returns 204 No Content whether the document existed or not.
     *
     * @param docId ID of document to remove
     */
    @ApiOperation(value = "Deletes a document",
            notes = "Succeeds whether or not the document already existed.",
            code = 204) // No Content
    // EXAMPLE NOTE: We should document response code(s) with an @ApiResponses annotation
    @ApiResponses({
        @ApiResponse(code = 204, message = "ExampleDocument is deleted"),
    })
    @DELETE @Path("{id: [0-9]+}")
    @Asap(authorizedIssuers = "tester")
    public void deleteExampleDocument(@PathParam("id") ExampleDocId docId) {
        controller.remove(docId);
    }

    /**
     * Updates the content of an existing document.
     *
     * <p>REST API note: returns 404 Not Found on no-such-document; 200 OK with the document content if the doc
     * exists and is updated.
     *
     * @param docId ID of existing document
     * @param newExampleDocument Updated document content
     * @return the updated document
     */
    @PUT @Path("{id: [0-9]+}")
    @Consumes(APPLICATION_JSON)
    @ApiOperation(value = "Updates the content of an existing document",
            notes = "Returns the document and the ID of the updated document")
    @ApiResponses({
        @ApiResponse(code = SC_OK, message = "When document is updated"),
        @ApiResponse(code = SC_NOT_FOUND, message = "When document does not exist"),
    })
    @Asap(authorizedIssuers = "tester")
    public ExampleDocumentResponse updateExampleDocument(@PathParam("id") ExampleDocId docId, ExampleDocument newExampleDocument) {
        if (!controller.containsKey(docId)) {
            throw new NotFoundException(format("Can't update document %s: it does not exist", docId));
        }
        controller.update(docId, newExampleDocument);
        return new ExampleDocumentResponse().setId(docId).setDoc(newExampleDocument);
    }

    // EXAMPLE NOTES: You might nest this class since it's only used by this resource.
    /**
     * The data-type of the response from GET, POST, and PUT
     */
    @SuppressWarnings("WeakerAccess") // Jackson constructs these
    public static class ExampleDocumentResponse {
        private ExampleDocId id;
        private ExampleDocument doc;

        @JsonIgnore
        public ExampleDocId getId() {
            return id;
        }

        @SuppressWarnings("unused") // Jackson uses it
        @JsonProperty("id")
        public long getIdAsLong() {
            return id.asLong();
        }

        public ExampleDocumentResponse setId(ExampleDocId id) {
            this.id = id;
            return this;
        }

        @SuppressWarnings("unused") // Jackson uses it
        public ExampleDocument getDoc() {
            return doc;
        }

        public ExampleDocumentResponse setDoc(ExampleDocument doc) {
            this.doc = doc;
            return this;
        }
    }

}

