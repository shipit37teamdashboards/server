package io.atlassian.myservice.resource;

import com.google.inject.Inject;
import io.atlassian.myservice.application.RequiresBasicHttpAuth;
import io.atlassian.myservice.clients.directory.DirectoryClient;
import io.atlassian.myservice.clients.directory.model.EmployeeDetails;
import io.atlassian.myservice.clients.directory.model.Service;
import io.atlassian.myservice.clients.projectcentral.ProjectCentralClient;
import io.atlassian.myservice.clients.projectcentral.model.ProjectsSearchResult;
import io.atlassian.myservice.resource.entities.ProjectDTO;
import io.atlassian.myservice.resource.entities.ServiceDTO;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.atlassian.myservice.util.DateUtil.parseDate;
import static io.atlassian.myservice.util.DateUtil.parseDayOfYear;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Path("/services")
@RequiresBasicHttpAuth
public class ServicesResource {

    private final DirectoryClient directoryClient;

    @Inject
    public ServicesResource(DirectoryClient directoryClient) {
        this.directoryClient = directoryClient;
    }

    @GET
    @Produces(APPLICATION_JSON)
    public List<ServiceDTO> getServices(@QueryParam("usernames") String usernamesStr,
                                                  @Context HttpServletRequest request) throws IOException {
        if (isEmpty(usernamesStr)) {
            throw new ValidationException("'usernames' query parameter containing the names of the users joined by ',' is expected!");
        }
        String[] usernames = StringUtils.split(usernamesStr, ',');
        Map<String, Service> services = new HashMap<>();
        for (String username : usernames) {
            EmployeeDetails employeeDetails = directoryClient.getEmployee(username).execute().body();
            for (Service service : employeeDetails.getServices()) {
                if (services.containsKey(service.getKey())) {
                    continue;
                }
                services.put(service.getKey(), service);
            }
        }

        return services.values().stream().map(s ->
                new ServiceDTO(s.getKey(), s.getName(), s.getDescription(), s.getUrl(), s.getServiceCatalogUrl(), s.getUsername()))
                .collect(Collectors.toList());
    }

}
