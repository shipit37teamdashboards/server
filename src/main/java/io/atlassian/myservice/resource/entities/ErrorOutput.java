package io.atlassian.myservice.resource.entities;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ErrorOutput {
    private final String message;
    private final List<String> details;
    private final int code;

    /**
     * Constructs an {@link ErrorOutput} from an exception.
     * @param exception the exception
     */
    public ErrorOutput(Exception exception, Response.StatusType status) {
        message = exception.toString();
        details = new ArrayList<>();
        code = status.getStatusCode();

        appendStackTraces(exception, details);
    }

    private void appendStackTraces(Throwable t, List<String> lines) {
        for (StackTraceElement elem : t.getStackTrace()) {
            lines.add(elem.toString());
        }
        if (t.getCause() != null) {
            lines.add("Caused by:");
            appendStackTraces(t.getCause(), lines);
        }
    }

    /**
     * Constructs an {@link ErrorOutput} from a status and a list of detailed messages.
     * @param status The status
     * @param details A {@link List} of messages
     */
    public ErrorOutput(Response.StatusType status, List<String> details) {
        this.message = status.getReasonPhrase();
        this.details = new ArrayList<>(details);
        this.code = status.getStatusCode();
    }

    public ErrorOutput(Response.StatusType status, String detail) {
        this.message = status.getReasonPhrase();
        this.details = Collections.singletonList(detail);
        this.code = status.getStatusCode();
    }

    public List<String> getDetails() {
        return Collections.unmodifiableList(details);
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
