package io.atlassian.myservice.resource.entities;

public class ServiceDTO {

    private final String key;
    private final String name;
    private final String decription;
    private final String url;
    private final String serviceCentralUrl;
    private final String owner;

    public ServiceDTO(String key, String name, String decription, String url, String serviceCentralUrl, String owner) {
        this.key = key;
        this.name = name;
        this.decription = decription;
        this.url = url;
        this.serviceCentralUrl = serviceCentralUrl;
        this.owner = owner;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getDecription() {
        return decription;
    }

    public String getUrl() {
        return url;
    }

    public String getServiceCentralUrl() {
        return serviceCentralUrl;
    }

    public String getOwner() {
        return owner;
    }
}
