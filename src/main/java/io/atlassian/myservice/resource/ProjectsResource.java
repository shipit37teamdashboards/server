package io.atlassian.myservice.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import io.atlassian.myservice.application.RequiresBasicHttpAuth;
import io.atlassian.myservice.clients.projectcentral.ProjectCentralClient;
import io.atlassian.myservice.clients.projectcentral.model.ProjectsSearchResult;
import io.atlassian.myservice.config.MyServiceConfiguration;
import io.atlassian.myservice.resource.entities.ProjectDTO;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static io.atlassian.myservice.util.DateUtil.parseDate;
import static io.atlassian.myservice.util.DateUtil.parseDayOfYear;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * @author Ilya Sadykov
 */
@Path("/projects")
@RequiresBasicHttpAuth
public class ProjectsResource {

    private final ObjectMapper objectMapper;
    private final MyServiceConfiguration configuration;

    @Inject
    public ProjectsResource(ObjectMapper objectMapper, MyServiceConfiguration configuration) {
        this.objectMapper = objectMapper;
        this.configuration = configuration;
    }

    @GET
    @Produces(APPLICATION_JSON)
    public Optional<List<ProjectDTO>> getProjects(@QueryParam("usernames") String usernames,
                                                  @Context HttpServletRequest request) throws IOException {
        if (isEmpty(usernames)) {
            throw new ValidationException("'usernames' query parameter containing the names of the users joined by ',' is expected!");
        }
        final ProjectCentralClient projectCentral = createRetrofit(request).create(ProjectCentralClient.class);
        final Call<ProjectsSearchResult> projectsCall = projectCentral.getProjectsByUsernames(usernames);
        final ProjectsSearchResult projects = projectsCall.execute().body();
        return Optional.of((projects).getProjects().stream().map((project) -> {
            final ProjectDTO dto = new ProjectDTO();
            dto.setName(project.getFields().getSummary());
            dto.setDescription(project.getFields().getDescription());
            dto.setEndDate(parseDayOfYear(project.getFields().getShipDate()).getTime());
            dto.setStartDate(parseDate(project.getFields().getCreated()).getTime());
            dto.setUpdateDate(parseDate(project.getFields().getUpdated()).getTime());
            dto.setStatus(project.getFields().getStatus().getName());
            dto.setTrackStatus(project.getFields().getTrackStatus());
            dto.setStage(project.getFields().getStage().getValue());
            dto.setOwner(project.getFields().getOwner().getName());
            dto.setUrl("https://extranet.atlassian.com/jira/browse/" + project.getKey());
            return dto;
        }).collect(toList()));
    }

    private Retrofit createRetrofit(HttpServletRequest request) {
        return new Retrofit.Builder()
                .baseUrl(configuration.getProjectcentral().getUrl())
                .client(new OkHttpClient.Builder().addInterceptor((chain) ->
                        chain.proceed(chain.request().newBuilder()
                                .addHeader("Authorization", request.getHeader("Authorization")).build()
                        )).build())
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .build();
    }

}
