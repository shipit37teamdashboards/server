package io.atlassian.myservice.clients.hipchat;

import ch.viascom.hipchat.api.HipChat;
import ch.viascom.hipchat.api.exception.APIException;
import ch.viascom.hipchat.api.models.Room;
import ch.viascom.hipchat.api.request.models.GetAllRooms;
import io.atlassian.myservice.clients.directory.model.TeamMemberList;
import io.evanwong.oss.hipchat.v2.HipChatClient;
import io.evanwong.oss.hipchat.v2.rooms.RoomItem;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * Created by akoshelev on 12/8/16.
 */
public class HipChatIntegration {
    private static final String HIPCHAT_TOKEN = "4SUdGIjdhiF0ME1gkZSAQm3fWbBRCu6C0A3ahHVT";
    private final HipChatClient userClient;
    private final HipChat roomClient;

    public HipChatIntegration() {
        userClient = new HipChatClient(HIPCHAT_TOKEN);
        roomClient = new HipChat(HIPCHAT_TOKEN);
    }

    public List<String> getRoomsForTeam(TeamMemberList memberList) {
        List<String> result = new LinkedList<>();
        List<Room> allPublicRooms;
        try {
            allPublicRooms = getAllPublicRooms();
        } catch (APIException ae) {
            return result;
        }

        for (int i = 0; i < 3; i++) {
            result.add(allPublicRooms.get(new Random().nextInt(allPublicRooms.size())).getName());
        }
        return result;
    }

    public String getLastActive(String email) throws ExecutionException, InterruptedException, APIException {
        int userId = userClient.prepareViewUserRequestBuilder(email).build().execute().get().getId();
        return roomClient.usersAPI().viewUser(Integer.toString(userId)).getLastActive();
    }

    private List<Room> getAllPublicRooms() throws APIException {
        return roomClient.roomsAPI().getAllRooms(
                new GetAllRooms(0, 5000, false, false)).getItems();
    }

}
