package io.atlassian.myservice.clients.atlaseye.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by akoshelev on 12/8/16.
 */
public class Repository {
    @JsonProperty("repository")
    private List<RepositoryItem> repositoryItems;

    public List<RepositoryItem> getItems() {
        return repositoryItems;
    }
}
