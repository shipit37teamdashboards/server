package io.atlassian.myservice.clients.atlaseye.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by akoshelev on 12/9/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryItem {
    @JsonProperty
    private String name;

    @JsonProperty
    private String displayName;

    @JsonProperty
    private boolean enabled;

    @JsonProperty
    private boolean finishedFullSlurp;

    @JsonProperty
    private String location;

    @JsonProperty
    private String repositoryState;

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isFinishedFullSlurp() {
        return finishedFullSlurp;
    }

    public String getLocation() {
        return location;
    }

    public String getRepositoryState() {
        return repositoryState;
    }
}
