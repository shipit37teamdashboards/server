package io.atlassian.myservice.clients.atlaseye;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.atlassian.myservice.clients.atlaseye.model.Repository;
import io.atlassian.myservice.clients.atlaseye.model.RepositoryItem;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by akoshelev on 12/8/16.
 */
public class AtlasEyeClient {
    private static List<RepositoryItem> allRepos;

    public static List<RepositoryItem> getReposForTeam() {
        if (allRepos == null) {
            allRepos = getAllRepos();
        }
        Integer reposNumber = new Random().nextInt(4) + 3;
        List<RepositoryItem> result = new LinkedList<>();
        for (int i = 0; i < reposNumber; i++) {
            result.add(allRepos.get(new Random().nextInt(allRepos.size())));
        }
        return result;
    }

    private static List<RepositoryItem> getAllRepos() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(
                    AtlasEyeClient.class.getClassLoader().getResource("repos.json"), Repository.class)
                    .getItems();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
