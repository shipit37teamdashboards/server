package io.atlassian.myservice.clients.directory.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Team {

    @JsonProperty("id")
    private long id;

    @JsonProperty("name")
    private String name;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
