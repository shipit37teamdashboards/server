package io.atlassian.myservice.clients.directory.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SitLocation {

    @JsonProperty("sit_location")
    private String floorPlan;

    @JsonProperty("floor_detail")
    private String floorDetail;

    @JsonProperty("x")
    private String x;

    @JsonProperty("y")
    private String y;

    public String getFloorPlan() {
        return floorPlan;
    }

    public String getFloorDetail() {
        return floorDetail;
    }

    public String getX() {
        return x;
    }

    public String getY() {
        return y;
    }
}
