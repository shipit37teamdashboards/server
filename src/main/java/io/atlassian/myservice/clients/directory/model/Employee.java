package io.atlassian.myservice.clients.directory.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Employee {

    @JsonProperty("username")
    private String username;

    @JsonProperty("full_name")
    private String fullName;

    @JsonProperty("title")
    private JobTitle jobTitle;

    @JsonProperty("department")
    private Team department;

    @JsonProperty("team")
    private Team team;

    @JsonProperty("manager_username")
    private String managerUsername;

    @JsonProperty("manager_full_name")
    private String managerFullName;

    @JsonProperty("hire_date")
    private String hireDate;

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return fullName;
    }

    public JobTitle getJobTitle() {
        return jobTitle;
    }

    public Team getDepartment() {
        return department;
    }

    public Team getTeam() {
        return team;
    }

    public String getManagerUsername() {
        return managerUsername;
    }

    public String getManagerFullName() {
        return managerFullName;
    }

    public String getHireDate() {
        return hireDate;
    }
}


