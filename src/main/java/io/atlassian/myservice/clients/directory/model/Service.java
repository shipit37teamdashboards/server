package io.atlassian.myservice.clients.directory.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Service {

    @JsonProperty("description")
    private String description;

    @JsonProperty("key")
    private String key;

    @JsonProperty("name")
    private String name;

    @JsonProperty("service_catalog_url")
    private String serviceCatalogUrl;

    @JsonProperty("url")
    private String url;

    @JsonProperty("username")
    private String username;

    public String getDescription() {
        return description;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getServiceCatalogUrl() {
        return serviceCatalogUrl;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }
}
