package io.atlassian.myservice.clients.directory.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JobTitle {

    @JsonProperty("id")
    private long id;

    @JsonProperty("name")
    private String name;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
