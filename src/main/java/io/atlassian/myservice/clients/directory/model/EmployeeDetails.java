package io.atlassian.myservice.clients.directory.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class EmployeeDetails extends Employee {

    @JsonProperty("sit_location")
    private SitLocation location;

    @JsonProperty("owned_services")
    private List<Service> services;

    public SitLocation getLocation() {
        return location;
    }

    public List<Service> getServices() {
        return services;
    }
}
