package io.atlassian.myservice.clients.directory.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TeamMemberList {

    @JsonProperty("count")
    private int count;

    @JsonProperty("page")
    private int page;

    @JsonProperty("results")
    private List<Employee> results;

    public int getCount() {
        return count;
    }

    public int getPage() {
        return page;
    }

    public List<Employee> getResults() {
        return results;
    }
}
