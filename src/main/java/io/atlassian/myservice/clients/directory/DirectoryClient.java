package io.atlassian.myservice.clients.directory;

import io.atlassian.myservice.clients.directory.model.EmployeeDetails;
import io.atlassian.myservice.clients.directory.model.TeamMemberList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DirectoryClient {

    @GET("staff/{username}")
    Call<EmployeeDetails> getEmployee(@Path("username") String username);

    @GET("teams/{teamId}/members")
    Call<TeamMemberList> getTeamMembers(@Path("teamId") long teamId);
}
