package io.atlassian.myservice.clients.projectcentral.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Ilya Sadykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectCustomField {
    String self;
    String value;
    String id;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
