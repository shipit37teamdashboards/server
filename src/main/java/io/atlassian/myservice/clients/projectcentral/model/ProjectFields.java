package io.atlassian.myservice.clients.projectcentral.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * @author Ilya Sadykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectFields {
    private ProjectStatus status;
    private String summary;
    @JsonProperty("customfield_23189")
    @JsonDeserialize(using = ProjectTrackStatusDeserializer.class)
    private String trackStatus;
    @JsonProperty("customfield_18982")
    private String shipDate;
    @JsonProperty("customfield_18980")
    private String description;
    private String created;
    private String updated;
    @JsonProperty("customfield_23188")
    private ProjectCustomField stage;
    @JsonProperty("customfield_23187")
    private ProjectOwner owner;

    public ProjectCustomField getStage() {
        return stage;
    }

    public void setStage(ProjectCustomField stage) {
        this.stage = stage;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public String getTrackStatus() {
        return trackStatus;
    }

    public void setTrackStatus(String trackStatus) {
        this.trackStatus = trackStatus;
    }

    public String getShipDate() {
        return shipDate;
    }

    public void setShipDate(String shipDate) {
        this.shipDate = shipDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public ProjectOwner getOwner() {
        return owner;
    }

    public void setOwner(ProjectOwner owner) {
        this.owner = owner;
    }
}
