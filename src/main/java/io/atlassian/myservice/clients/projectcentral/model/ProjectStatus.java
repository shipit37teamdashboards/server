package io.atlassian.myservice.clients.projectcentral.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Ilya Sadykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectStatus {
    String description;
    String iconUrl;
    String name;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
