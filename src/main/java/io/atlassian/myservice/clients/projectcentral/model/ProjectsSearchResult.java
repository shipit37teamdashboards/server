package io.atlassian.myservice.clients.projectcentral.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ilya Sadykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectsSearchResult {

    @JsonProperty("issues")
    List<ProjectDetails> projects = new ArrayList<>();

    public List<ProjectDetails> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectDetails> projects) {
        this.projects = projects;
    }
}
