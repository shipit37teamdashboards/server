package io.atlassian.myservice.clients.projectcentral.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Ilya Sadykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectOwner {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
