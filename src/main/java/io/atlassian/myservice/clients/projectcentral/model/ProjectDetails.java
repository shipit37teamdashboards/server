package io.atlassian.myservice.clients.projectcentral.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectDetails {
    private String key;
    private ProjectFields fields;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ProjectFields getFields() {
        return fields;
    }

    public void setFields(ProjectFields fields) {
        this.fields = fields;
    }
}
