package io.atlassian.myservice.clients.projectcentral;

import io.atlassian.myservice.clients.projectcentral.model.ProjectsSearchResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ProjectCentralClient {

    default Call<ProjectsSearchResult> getProjectsByUsernames(String usernames) {
        return getProjectsByJql("Project%20=%20PC%20AND%20\"Who%20is%20the%20full-time%20owner?\"%20in%20("+usernames+")");
    }

    @GET("search")
    Call<ProjectsSearchResult> getProjectsByJql(@Query(value = "jql", encoded = true) String jql);
}
