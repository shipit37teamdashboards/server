package io.atlassian.myservice.clients.asap;


import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.client.http.AuthorizationHeaderGeneratorImpl;
import com.atlassian.asap.core.keys.privatekey.PrivateKeyProviderFactory;
import com.atlassian.asap.nimbus.serializer.NimbusJwtSerializer;
import io.atlassian.dropwizard.asap.AsapConfiguration;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class AsapRequestInterceptor implements Interceptor {
    private final AsapConfiguration asapConfiguration;
    private final String audience;
    private final AuthorizationHeaderGenerator authorizationHeaderGenerator;

    public AsapRequestInterceptor(AsapConfiguration asapConfiguration, String audience) {
        this.asapConfiguration = asapConfiguration;
        this.authorizationHeaderGenerator = new AuthorizationHeaderGeneratorImpl(new NimbusJwtSerializer(),
                PrivateKeyProviderFactory.createPrivateKeyProvider(asapConfiguration.getOutbound().privateKeyUrl()));
        this.audience = audience;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Request asapRequest = originalRequest.newBuilder()
                .header("Authorization", generateAsapHeader())
                .build();
        return chain.proceed(asapRequest);
    }

    private String generateAsapHeader() {
        Jwt token = JwtBuilder.newJwt()
                .keyId(asapConfiguration.getOutbound().keyId())
                .issuer(asapConfiguration.getOutbound().issuer())
                .audience(audience)
                .build();
        try {
            return authorizationHeaderGenerator.generateAuthorizationHeader(token);
        } catch (InvalidTokenException | CannotRetrieveKeyException e) {
            throw new RuntimeException("Failed to generate ASAP header", e);
        }
    }
}
