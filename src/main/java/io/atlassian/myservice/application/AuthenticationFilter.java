package io.atlassian.myservice.application;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * This filter verify the access permissions for a user
 * based on username and passowrd provided in request
 */
@RequiresBasicHttpAuth
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    public static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED)
            .header("WWW-Authenticate", "Basic")
            .entity("You cannot access this resource").build();

    @Override
    public void filter(ContainerRequestContext requestContext) {
        //Fetch authorization header
        final List<String> authorization = requestContext.getHeaders().get(AUTHORIZATION_PROPERTY);

        //If no authorization information present; block access
        if (authorization == null || authorization.isEmpty()) {
            requestContext.abortWith(ACCESS_DENIED);
        }
    }
}