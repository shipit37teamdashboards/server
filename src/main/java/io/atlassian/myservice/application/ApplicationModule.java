package io.atlassian.myservice.application;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import io.atlassian.myservice.clients.directory.DirectoryClient;
import io.atlassian.myservice.clients.projectcentral.ProjectCentralClient;
import io.atlassian.myservice.config.MyServiceConfiguration;
import io.atlassian.myservice.config.ServiceConfig;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.setup.Environment;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ApplicationModule extends AbstractModule {

    @Override
    protected void configure() {
        // TODO: register services here
    }

    @Provides
    public MetricRegistry getMetricRegistry(final Environment environment) {
        return environment.metrics();
    }

    @Provides
    @Singleton
    public ObjectMapper provideObjectMapper() {
        return Jackson.newObjectMapper()
                .setDateFormat(new ISO8601DateFormat())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Provides
    public DirectoryClient getDirectoryClient(ObjectMapper objectMapper, MyServiceConfiguration configuration) {
        Retrofit retrofit = createRetrofit(objectMapper, configuration.getDirectory());
        return retrofit.create(DirectoryClient.class);
    }

    @Provides
    public ProjectCentralClient getProjectCentralClient(ObjectMapper objectMapper, MyServiceConfiguration configuration) {
        Retrofit retrofit = createRetrofit(objectMapper, configuration.getProjectcentral());
        return retrofit.create(ProjectCentralClient.class);
    }

    private static Retrofit createRetrofit(ObjectMapper objectMapper, ServiceConfig serviceConfig) {
        return new Retrofit.Builder()
                .baseUrl(serviceConfig.getUrl())
                .client(new OkHttpClient.Builder().build())
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .build();
    }

    // TODO(nislamov): The code below relies on assumption that Directory supports ASAP

//    private static Retrofit createRetrofit(ServiceConfig serviceConfig, AsapConfiguration asapConfiguration) {
//        return new Retrofit.Builder()
//                .baseUrl(serviceConfig.getUrl())
//                .client(createAsapHttpClient(serviceConfig, asapConfiguration))
//                .addConverterFactory(JacksonConverterFactory.create())
//                .build();
//    }

//    private static OkHttpClient createAsapHttpClient(ServiceConfig serviceConfig, AsapConfiguration asapConfiguration) {
//        return new OkHttpClient.Builder()
//                .addInterceptor(new AsapRequestInterceptor(asapConfiguration, serviceConfig.getAsapAudience()))
//                .build();
//    }
}

