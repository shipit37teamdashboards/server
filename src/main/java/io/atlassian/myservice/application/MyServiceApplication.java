package io.atlassian.myservice.application;

import com.google.inject.Injector;
import com.hubspot.dropwizard.guice.GuiceBundle;
import io.atlassian.dropwizard.application.MicrosApplication;
import io.atlassian.myservice.config.MyServiceConfiguration;
import io.atlassian.myservice.controller.ExampleDocumentControllerImpl;
import io.atlassian.myservice.resource.*;
import io.atlassian.myservice.resource.exceptions.GlobalExceptionMapper;
import io.atlassian.myservice.resource.exceptions.JerseyViolationExceptionMapper;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class MyServiceApplication extends MicrosApplication<MyServiceConfiguration> {
    private GuiceBundle<MyServiceConfiguration> guiceBundle;

    @Override
    public String getName() {
        return "shipit-beehives";
    }


    @Override
    public void postRun(MyServiceConfiguration configuration, Environment environment) throws Exception {

        // required CORS support
        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter("allowedOrigins", "*.atlassian.io"); // allowed origins comma separated
        filter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        filter.setInitParameter("allowedMethods", "GET,PUT,POST,DELETE,OPTIONS,HEAD");
        filter.setInitParameter("preflightMaxAge", "5184000"); // 2 months
        filter.setInitParameter("allowCredentials", "true");

        JerseyEnvironment jersey = environment.jersey();
        Injector injector = guiceBundle.getInjector();
        //TODO: When the application runs, this is called after the default Bundles are run.
        // Override it to add providers, resources, etc. for your application.

        environment.jersey().register(new DocsExampleResource(new ExampleDocumentControllerImpl()));
        jersey.register(injector.getInstance(TeamResource.class));
        jersey.register(injector.getInstance(ProjectsResource.class));
        jersey.register(injector.getInstance(ServicesResource.class));
        jersey.register(injector.getInstance(AuthenticationFilter.class));
        jersey.register(injector.getInstance(RepositoriesResource.class));

        jersey.register(injector.getInstance(GlobalExceptionMapper.class));
        jersey.register(injector.getInstance(JerseyViolationExceptionMapper.class));
        environment.jersey().setUrlPattern("/api/*");
    }

    @Override
    public void preInitialize(Bootstrap<MyServiceConfiguration> bootstrap) {

        guiceBundle = GuiceBundle.<MyServiceConfiguration>newBuilder()
                .addModule(new ApplicationModule())
                .enableAutoConfig(getClass().getPackage().getName())
                .setConfigClass(MyServiceConfiguration.class)
                .build();

        bootstrap.addBundle(guiceBundle);
        bootstrap.addBundle(new AssetsBundle("/assets", "/", "index.html"));
    }


}

