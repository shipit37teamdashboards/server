package io.atlassian.myservice.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceConfig {

    @JsonProperty
    private String url;

    @JsonProperty
    private String asapAudience;

    public String getUrl() {
        return url;
    }

    public String getAsapAudience() {
        return asapAudience;
    }
}
