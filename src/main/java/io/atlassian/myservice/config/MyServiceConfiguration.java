package io.atlassian.myservice.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.atlassian.dropwizard.config.MicrosConfiguration;

@SuppressWarnings("unused")
public class MyServiceConfiguration extends MicrosConfiguration {

    @JsonProperty
    private ServiceConfig directory;

    @JsonProperty
    private ServiceConfig projectcentral;

    @JsonProperty
    private ServiceConfig repositories;

    public MyServiceConfiguration() {
    }

    public ServiceConfig getDirectory() {
        return directory;
    }

    public ServiceConfig getProjectcentral() {
        return projectcentral;
    }

    public ServiceConfig getRepositories() {
        return repositories;
    }
}

