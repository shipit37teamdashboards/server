package io.atlassian.myservice;

import io.atlassian.dropwizard.application.ApplicationLauncher;
import io.atlassian.dropwizard.application.ApplicationLauncher.ConfigMode;
import io.atlassian.myservice.application.MyServiceApplication;

public class Main {

    public static void main(String[] args) throws Exception {
        MyServiceApplication application = new MyServiceApplication();

        ApplicationLauncher.launch(ConfigMode.MERGING, "shipit-beehives", application, args, null);
    }
}

