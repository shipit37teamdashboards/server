package io.atlassian.myservice.controller;

/**
 * The data-type to represent the document at the REST API level; and, using Jackson, also defines the JSON serialization
 * of the document.
 */
@SuppressWarnings("WeakerAccess") // Jackson constructs these
public class ExampleDocument {
    @SuppressWarnings("unused")  // Jackson assigns this
    private String author;
    @SuppressWarnings("unused")  // Jackson assigns this
    private String content;

    @SuppressWarnings("unused")  // Jackson uses it
    public ExampleDocument() {}

    public ExampleDocument(String author, String content) {
        this.author = author;
        this.content = content;
    }

    @SuppressWarnings("unused")  // Jackson uses it
    public String getAuthor() {
        return author;
    }

    @SuppressWarnings("unused")  // Jackson uses it
    public String getContent() {
        return content;
    }
}

