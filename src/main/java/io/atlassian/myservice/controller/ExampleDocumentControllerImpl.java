package io.atlassian.myservice.controller;


import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.Map;
import javax.annotation.Nullable;

/**
 * Controller's purpose is to be a middleware between Jersey resource (REST API) and data storage
 */
public class ExampleDocumentControllerImpl implements ExampleDocumentController {

    // EXAMPLE NOTES: This is just an example of data store. Should be replaced with a real one.
    // You couldn't use this in production use because it keeps state from request-to-request,
    // but that state isn't shared in a database between instances of this application

    private final AtomicLong nextExampleDocId = new AtomicLong(1L);

    // ExampleDocument store, map of docId to doc
    private final Map<ExampleDocId, ExampleDocument> data = new ConcurrentHashMap<>();

    @Override
    public ExampleDocId insert(ExampleDocument doc) {
        ExampleDocId docId = new ExampleDocId(nextExampleDocId.getAndIncrement());
        data.put(docId, doc);
        return docId;
    }

    @Override
    public void update(ExampleDocId docId, ExampleDocument doc) {
        data.put(docId, doc);
    }

    @Override
    @Nullable
    public ExampleDocument get(ExampleDocId docId) {
        return data.get(docId);
    }

    @Override
    public void remove(ExampleDocId docId) {
        data.remove(docId);
    }

    @Override
    public boolean containsKey(ExampleDocId docId) {
        return data.containsKey(docId);
    }


}

