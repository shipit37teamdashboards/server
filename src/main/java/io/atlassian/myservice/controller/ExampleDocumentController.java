package io.atlassian.myservice.controller;

import javax.annotation.Nullable;

/**
 * Controller's purpose is to be a middleware between Jersey resource (REST API) and data storage
 */
public interface ExampleDocumentController {

    ExampleDocId insert(ExampleDocument doc);

    void update(ExampleDocId docId, ExampleDocument doc);

    @Nullable
    ExampleDocument get(ExampleDocId docId);

    void remove(ExampleDocId docId);

    boolean containsKey(ExampleDocId docId);

}

