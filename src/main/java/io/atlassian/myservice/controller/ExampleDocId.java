package io.atlassian.myservice.controller;

import java.util.Objects;

/**
 * The document id primary key (type-safe integer wrapper).
 *
 * This is a parameter type supported by Jersey because it meets the contracts defined here:
 * See https://jersey.java.net/documentation/latest/jaxrs-resources.html#d0e2193
 */
@SuppressWarnings("WeakerAccess") // Jersey needs this to be public
public class ExampleDocId {
    private final long docId;

    @SuppressWarnings("unused") // Jersey uses this
    public ExampleDocId(String docId) {
        this.docId = Long.parseLong(docId);
    }

    public ExampleDocId(long docId) {
        this.docId = docId;
    }

    public long asLong() {
        return docId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExampleDocId other = (ExampleDocId) o;
        return docId == other.docId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(docId);
    }

    @Override
    public String toString() {
        return String.valueOf(docId);
    }
}

