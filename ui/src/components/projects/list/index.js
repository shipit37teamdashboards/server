import React from 'react';
import { Link } from 'react-router';

const ProjectList = (props, context) => {
    return (
        <ul className="aui-nav">
            {Array.isArray(context.state.projects) && context.state.projects.map(
                project => (<li key={project.uuid}><Link>{project.name}</Link></li>)
            )}
        </ul>
    );
};

ProjectList.contextTypes = {
    state: React.PropTypes.object
};

ProjectList.propTypes = {
};

export default ProjectList;