import React from 'react';

const TeamField = (props, context) => {
    return (<span>{context.state['team'][props.name]}</span>);
};

TeamField.contextTypes = {
    state: React.PropTypes.object
};

TeamField.propTypes = {
    name: React.PropTypes.string.isRequired
};

export default TeamField;