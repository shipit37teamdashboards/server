import React from 'react';

const TeamLocation = (props, context) => {
    let i = 0;
    let location = (context.state.team && context.state.team.location) || {};
    if (!Array.isArray(location)) {
        location = [location];
    }
    return (
        <div className="team-location">
            { location.map(location =>
                (<div className="beehive-map map-team-location" key={i++}>
                    <h2>Location: {location.floorTitle}</h2>
                    <svg width="600" height="200">
                        <image xlinkHref={location.floorPlanUrl} width="600" height="200"/>
                        <path fill="#F99" fillOpacity="0.5" d={location.polygon} style={{cursor: "pointer"}}>
                            <title>Team location:</title>
                        </path>
                    </svg>
                </div>)
            )}
        </div>
    );
};

TeamLocation.contextTypes = {
    state: React.PropTypes.object
};

TeamLocation.propTypes = {};

export default TeamLocation;