import React from 'react';

const hiplink = (roomName) => {
    return 'hipchat://atlassian.hipchat.com/' + encodeURIComponent(roomName);
};

const TeamContacts = (props, context) => {
    let i = 0;
    const hrooms = (context.state.team && context.state.team.hipchatRooms) || [];
    return (<div>
        <ul className="aui-nav">
            <li><a><h6>Hipchat:</h6></a></li>
            {hrooms.map(
                room => (<li key={i++}><a href={hiplink(room)}>{room}</a></li>)
            )}
        </ul>
    </div>);
};

TeamContacts.contextTypes = {
    state: React.PropTypes.object
};

TeamContacts.propTypes = {};

export default TeamContacts;