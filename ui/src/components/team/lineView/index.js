import React from 'react';
import Contact from './../memberContact';

import './team.css';

const TeamLine = (props, context) => {

    const members = (context.state.team && context.state.team.members) || [];

    let i = 0;
    return (<ul className="teamView teamLinearView">
        {members
            .sort((a, b)=> a.name[0] > b.name[0])
            .map(
                mate => {
                    let className = 'team-member-normal';
                    let bage = (i++) % 2 === 0
                        ? <span className="aui-lozenge aui-lozenge">out of office</span>
                        : undefined;//span className="aui-lozenge aui-lozenge-success">in place</span>;
                    if (Math.random() < 0.2) {
                        bage = <span className="aui-lozenge aui-lozenge-moved">vacation</span>;
                        className = 'team-member-vacation';
                    }
                    if (Math.random() < 0.2) {
                        bage = <span className="aui-lozenge aui-lozenge-current">ill</span>;
                        className = 'team-member-ill';
                    }

                    const hipchat = <Contact mate={mate}/>;

                    return (<li key={mate.username} className={className}>
                        <perks {...mate.perks} />
                        <bage>{bage}</bage>
                        <span className="aui-avatar aui-avatar-project aui-avatar-xlarge">
                            <span className="aui-avatar-inner"
                                  style={{backgroundImage: `url(${mate.photoUrl})`}}></span>
                        </span>
                        <name><a href={`https://directory.atlassian.io/employees/${mate.username}`}>{mate.name}</a></name>
                        <role>{mate.position}</role>
                        <hipchat>{hipchat}</hipchat>
                    </li>)
                }
            )}
    </ul>);
};

TeamLine.contextTypes = {
    state: React.PropTypes.object
};

export default TeamLine;