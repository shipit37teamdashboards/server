import React from 'react';
import Services from './../../components/services/list';

const Page = () => {
    return (
        <div>
            <h1>Services</h1>
            <Services />
        </div>
    );
};

export default Page;