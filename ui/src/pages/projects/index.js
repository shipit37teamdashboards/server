import React from 'react';
import {generateCellVerticalImage} from '../../utils/cellImage';
import './projects.css';

const factor = 10 / (24 * 60 * 60 * 1000); // day

const calStyle = (project, date) => {
    let className = 'green';
    const left = factor * (project.startDate - date);
    const right = factor * (project.endDate - date);
    let css = {
        left: left,
        width: right - left
    };
    return {
        css,
        className: `project-class${className}`
    }
};

const writeOwner = (context, project)=> {
    const members = context.state.team.members.filter(member=>member.username == project.owner);
    if (members.length == 1) {
        const mate = members[0];
        return <div>{project.owner}
            <span className="aui-avatar aui-avatar-project aui-avatar-xsmall">
              <span className="aui-avatar-inner" style={{backgroundImage: `url(${mate.photoUrl})`}}></span>
           </span>
        </div>
    } else
        return <div>{project.owner}</div>

}


const Page = (props, context) => {
    const forDate = Date.now();
    let i = 0;
    return (
        <div>
            <h1>Projects</h1>
            <div className="beehive-projects-root">
                <ul className="beehive-projects" style={{backgroundImage: `url(${generateCellVerticalImage(10)})`}}>
                    {Array.isArray(context.state.projects) && context.state.projects.map(
                        project => {
                            const style = calStyle(project, forDate);
                            const startDate = new Date(project.startDate);
                            const endDate = new Date(project.endDate);

                            const status = <span className="aui-lozenge aui-lozenge-success">{project.status}</span>;
                            const trackStatus = project.trackStatus === 'On Track'
                                ? <span className="aui-lozenge aui-lozenge-success">{project.trackStatus}</span>
                                : <span className="aui-lozenge aui-lozenge-current">{project.trackStatus}</span>;

                            const stage = project.stage === 'Make it'
                                ? <span className="aui-lozenge aui-lozenge-success">{project.stage}</span>
                                : <span className="aui-lozenge aui-lozenge-current">{project.stage}</span>;

                            return (<li key={i++} className={style.className}>
                                <div className="projectBar" style={style.css}>
                                    <label>{`${startDate.getDay()}.${startDate.getMonth()} /${project.name}/ ${endDate.getDay()}.${endDate.getMonth()}`}</label>
                                </div>
                                <label>{project.name}</label>
                                <tips>
                                    <div>{status}</div>
                                    <div>{trackStatus}</div>
                                    <div>{stage}</div>
                                </tips>
                                <owner>
                                    {writeOwner(context, project)}
                                </owner>
                            </li>)
                        }
                    )}
                </ul>
                <div className="todayLine todayLine-vertical"></div>
            </div>
        </div>
    );
};

Page.contextTypes = {
    state: React.PropTypes.object
};

Page.propTypes = {};


export default Page;