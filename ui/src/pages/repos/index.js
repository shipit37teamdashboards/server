import React from 'react';
import Repos from './../../components/repos/list';

const Page = () => {
    return (
        <div>
            <h1>Repos</h1>
            <Repos />
        </div>
    );
};

export default Page;