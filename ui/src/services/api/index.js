import createAPI from 'unity-api';

import team from './resources/teams';
import projects from './resources/projects';

export default createAPI({ team, projects }, [], '/api');