import React, {Component} from 'react';

import Sidebar from './components/ui/sidebar';
import Header from './components/ui/header';

import createRoutes from './createRoutes';
import createHistory from './createHistory';
import hashHistory from 'history/lib/createHashHistory';
import {Router} from 'react-router';

import defaultState from './state.json';

import teamList from './mock/teamList.json';

import mockTeam99 from './mock/team99.json';
import mockProjects from './mock/projects.json';
import mockServices from './mock/services.json';

defaultState.team = mockTeam99;
defaultState.teamList = teamList;
defaultState.projects = mockProjects;
defaultState.services = mockServices;

import API from './services/api';

import './App.css';

const Loader = () => (
    <div style={{}}>
        <div id="some-id" className="aui-progress-indicator">
            <span className="aui-progress-indicator-value"></span>
        </div>
    </div>
);

const Page = (props, context) => (
    <section id="content" role="main">
        <div className="aui-page-panel">
            <Header />
            {context.state.loading ? <Loader/> : undefined }
            <div
                className="aui-page-panel-inner"
                style={{
                    opacity: context.state.loading ? 0.7 : 1
                }}
            >
                <aside className="aui-page-panel-sidebar">
                    <Sidebar />
                </aside>
                <section className="aui-page-panel-content">
                    {props.children}
                </section>
            </div>
        </div>
    </section>
);

Page.propTypes = {
    children: React.PropTypes.node,
};

Page.contextTypes = {
    state: React.PropTypes.object,
};


const routes = createRoutes(Page);
const history = createHistory(hashHistory, routes);

class App extends Component {
    constructor() {
        super();
        this.setState = this.setState.bind(this);
        this.API = this.createAPI();
    }

    createAPI() {
        return {
            team: {
                get: (...args) => {
                    this.setState({loading: true});

                    const getTeam = API.team.get(...args)
                        .then(team => {
                            this.setState({team, teamId: team.teamId, loading: false, loaded: true})
                            return team;
                        })

                    const getProjects = getTeam
                        .then(team => {
                            const username = team.members.reduce((result, item) => {
                                result.push(item.username);
                                return result;
                            }, [])
                            return API.projects.getByUsername({username})
                        })
                    .then(projects => this.setState({projects}))
                }
            }
        }
    }

    componentDidMount() {
        this.setState({
            route: '',
            teamId: 0,
            ...defaultState,
            //loaded: true,
            //loading: false
        });

        this.API.team.get({id: 55});
    }

    getChildContext() {
        return {
            routeTo: (url) => this.setState({route: url}),
            state: this.state,
            API: this.API
        };
    }

    render() {
        const content = this.state && this.state.loaded && <Router history={history} routes={routes}/>;
        return content ? content : <div>loading...</div>;
    }
}


App.childContextTypes = {
    routeTo: React.PropTypes.func,
    state: React.PropTypes.object,
    API: React.PropTypes.object
};

export default App;