# Welcome to ShipIt Beehives!
This is a Java Dropwizard project that can be used build a Micros service with
compatible health checks and json logging, in addition to some ready-to-use
features such as configuration, metrics, REST mappings, ASAP service
authentication, etc.

# Links
- [Live service in ddev](http://example.com)
  - [deep health check](http://example.com/admin/healthcheck)
  - [swagger UI](http://example.com/swagger)
- [Source repository](http://stash.atlassian.com)
- [Micros UI](https://statlas.atlassian.io/micros-ui/index.html#/service/shipit-beehives)

# Features
- Lightweight and small.
- Json logging compatible with Micros. The json structure can be customized.
- Healthcheck (lightweight ping) at /admin/ping
- Deep Healthcheck (verifies resources are working) at /admin/healthcheck
- Thread dump at /admin/threads
- Application configuration in YAML files, with overrides possible for each Micros environment (adev, ddev, etc.)


# Technology stack

## Production Code
- Dropwizard provides Jetty as a web server.
- [Guice](https://github.com/google/guice) for dependency injection.



## Testing technology
- [AssertJ](http://joel-costigliola.github.io/assertj/) for fluent assertions
- JUnit 4
- [Dropwizard-testing](http://www.dropwizard.io/1.0.0/docs/manual/testing.html) library


## Development
- Java 8
- Gradle for builds


# Developing

## Prerequisites

If you've never used Gradle at Atlassian you'll, need to follow
[these instructions](https://extranet.atlassian.com/display/RELENG/HOWTO+-+Setting+up+gradle)
to get access to the right maven repositories.


## First run

As soon as you've got your code generated from Instant Micros, you should run
`bin/first_run` to finalize the installation.  (You can remove this section
from the README when you're done.)
(If [IM-183](https://sdog.jira-dev.com/browse/IM-183) is fixed, this won't be necessary.)

## Building

```bash
./gradlew build
```


## Preparing for MICROS deploy

This will bundle your app into a "fat jar" and generate the service descriptor:

```bash
bin/build
```

## Testing with ASAP

To test your ASAP integration, first run this script to install asap-cli and generate some config files.
```bash
bin/setup_asap_cli.sh
```

You can then use `asap curl` or `asap http` to make authenticated requests to your service. For example:

The example resource is configured to allow any validated ASAP request to read entities, but
only a service called "tester" is allowed to write them.

First start your service:
```bash
bamboo_maven_user=bamboo bamboo_maven_password=bamboo bin/build
java -jar shipit-beehives-0.0.1-*-fat.jar
```

Then in another terminal, access it
```bash
# Create a document as "tester". Should succeed:
asap --config-file .asap-config-tester  http -v POST http://localhost:8080/docs author=you content=something

# Try to create a document as the service talking to itself.  Should be rejected:
asap --config-file .asap-config-service-to-self  http -v POST http://localhost:8080/docs author=david content=something

# Try to read the document as "tester". Should succeed:
asap --config-file .asap-config-tester  http -v http://localhost:8080/docs/1000

# Try to read the document as the service.  Should also succeed:
asap --config-file .asap-config-service-to-self  http -v http://localhost:8080/docs/1000

# Try to read the document without an authorization header.  Should fail.
http -v http://localhost:8080/docs/1000
```

If you don't want to require ASAP headers for development, you can change `disableAsap: false` to `disableAsap: true`
in [shipit-beehives-local.yaml](src/main/resources/shipit-beehives-local.yaml).  Then all ASAP headers will be ignored.



