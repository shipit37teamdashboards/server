#!/bin/sh
set -e

./gradlew -I ./bamboo-support/init.gradle -P"versionSuffix=$bamboo_buildNumber" clean build serviceBundle

mv build/micros/*.yaml service-descriptor.yaml
mv build/micros/*.jar .
